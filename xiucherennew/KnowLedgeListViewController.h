//
//  KnowLedgeListViewController.h
//  xiucherennew
//
//  Created by harry on 13-11-11.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KnowLedgeListViewController : UIViewController
@property (assign,nonatomic) int typeseq;
@property (assign,nonatomic) NSString *typesname;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITableView *tbview;
- (IBAction)clickBack:(id)sender;
@end
