//
//  ResentViewController.h
//  xiucherennew
//
//  Created by harry on 13-12-7.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Post.h"

@interface ResentViewController : UIViewController<UIAlertViewDelegate>
@property (strong, nonatomic) NSString *postGuid;
@property (strong, nonatomic) Post *data;
@property (weak, nonatomic) IBOutlet GCPlaceholderTextView *txtContent;

- (IBAction)clickCancel:(id)sender;
- (IBAction)clickSend:(id)sender;
- (IBAction)clickAt:(id)sender;
@end