//
//  MoreSixinViewController.m
//  xiucherennew
//
//  Created by harry on 13-11-16.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import "MoreSixinViewController.h"
#import "MessageFrame.h"
#import "Message.h"
#import "MessageCell.h"

@interface MoreSixinViewController ()
{
    NSMutableArray  *_allMessagesFrame;
}
@end

@implementation MoreSixinViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    [_lblName setText:_nickname];
    _allMessagesFrame = [[NSMutableArray alloc] init];
    // Do any additional setup after loading the view from its nib.
   
    [self loadDailog];
    
    [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(loadDailog) userInfo:nil repeats:YES];

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)loadDailog{
    [_allMessagesFrame removeAllObjects];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *string = [NSString stringWithFormat:@"%@%@%@/%@",WEBSERVICE_URL,WEBSERVICE_getPrivateMsg,[AppDelegate getAppDelegate].userCode,_usercode];
    
    [manager GET:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        NSArray *resarr = [responseObject objectForKey:@"resultList"];
        NSString *previousTime = nil;
        for (int i=0; i<[resarr count]; i++) {
            MessageFrame *messageFrame = [[MessageFrame alloc] init];
            Message *message = [[Message alloc] init];
            message.dict = [resarr objectAtIndex:i];
            
            NSDictionary *d = [resarr objectAtIndex:i];
            NSString *curuser = d[@"scmePerson"];
            int ismy = [curuser isEqualToString:[AppDelegate getAppDelegate].userCode]?0:1;
            message.type = ismy;
            
            NSDictionary *curudic = [AppDelegate getAppDelegate].userDict;
            message.icon = ismy?_headimg:[curudic objectForKey:@"userHeadImagePath"];
            messageFrame.showTime = ![previousTime isEqualToString:message.time];
            
            messageFrame.message = message;
            
            previousTime = message.time;
            
            [_allMessagesFrame addObject:messageFrame];
            
        }
        [self.tableView reloadData];
        
        
        if (_allMessagesFrame.count>0) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_allMessagesFrame.count - 1 inSection:0];
            [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        }
        

        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    //[_txtContent becomeFirstResponder];
    
    NSLog(@"ScreenHeight:%f",ScreenHeight);
    if (ScreenHeight==480.f) {
        CGRect txtrect = _txtContent.frame;
        txtrect.origin.y-=88;
        _txtContent.frame = txtrect;

        CGRect tbrect = _tableView.frame;
        tbrect.size.height-=88;
        _tableView.frame = tbrect;

    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickCancel:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickSend:(id)sender {
    NSDictionary *dict = @{@"scmeParentGuid": @"",@"scmeTitle": @"",@"scmeContent": _txtContent.text,@"scmePerson": [AppDelegate getAppDelegate].userCode};
    NSString *output = [dict JSONRepresentation];
    
    NSArray *recuser = @[_usercode];
    NSString *recuserjson = [recuser JSONRepresentation];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *string = [NSString stringWithFormat:@"%@%@%@/%@",WEBSERVICE_URL,WEBSERVICE_savePrivateMsg,[output urlencode],[recuserjson urlencode]];
    NSLog(@"string:%@",string);
    [manager POST:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        NSString *result = [responseObject objectForKey:@"resultDataType"];
        
        NSLog(@"jsonArray: %@", result);
        
        if([result isEqualToString:@"1"]){
            //NSLog(@"to reload");
            //_txtContent.text = @"";
            
            [self loadDailog];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}



#pragma mark - 键盘处理
#pragma mark 键盘即将显示
- (void)keyBoardWillShow:(NSNotification *)note{
    
    CGRect rect = [note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat ty = - rect.size.height;
    [UIView animateWithDuration:[note.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue] animations:^{
        self.view.transform = CGAffineTransformMakeTranslation(0, ty);
        
//        CGRect txtrect = _txtContent.frame;
//        txtrect.origin.y = self.view.frame.size.height-txtrect.size.height-5;
//        _txtContent.frame = txtrect;
//
//        CGRect tbrect = _tableView.frame;
//        tbrect.size.height = self.view.frame.size.height - _txtContent.frame.origin.y-5;
//        _tableView.frame = tbrect;
        
//        CGRect txtrect = self.view.frame;
//        txtrect.origin.y = ty;
//        self.view.frame = txtrect;
        
        if (ScreenHeight==480.f) {
            CGRect txtrect = _txtContent.frame;
            txtrect.origin.y-=88;
            _txtContent.frame = txtrect;
            
            CGRect tbrect = _tableView.frame;
            tbrect.size.height-=88;
            _tableView.frame = tbrect;
            
        }

    }];
    
    
    

    
}
#pragma mark 键盘即将退出
- (void)keyBoardWillHide:(NSNotification *)note{
    
    [UIView animateWithDuration:[note.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue] animations:^{
        self.view.transform = CGAffineTransformIdentity;
        
//        CGRect txtrect = _txtContent.frame;
//        txtrect.origin.y = self.view.frame.size.height-txtrect.size.height+45;
//        _txtContent.frame = txtrect;
//        
//        CGRect tbrect = _tableView.frame;
//        tbrect.size.height = self.view.frame.size.height - _txtContent.frame.origin.y+45;
//        _tableView.frame = tbrect;
    }];
    
    
    

}
#pragma mark - 文本框代理方法
#pragma mark 点击textField键盘的回车按钮
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    /*
    // 1、增加数据源
    NSString *content = textField.text;
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    NSDate *date = [NSDate date];
    fmt.dateFormat = @"MM-dd"; // @"yyyy-MM-dd HH:mm:ss"
    NSString *time = [fmt stringFromDate:date];
    [self addMessageWithContent:content time:time];
     */
    [self clickSend:self];
    // 2、刷新表格
//    [self.tableView reloadData];
//    // 3、滚动至当前行
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_allMessagesFrame.count - 1 inSection:0];
//    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
//    // 4、清空文本框内容
//    _txtContent.text = nil;
    _txtContent.text = @"";
    return YES;
}

#pragma mark 给数据源增加内容
- (void)addMessageWithContent:(NSString *)content time:(NSString *)time{
    
    MessageFrame *mf = [[MessageFrame alloc] init];
    Message *msg = [[Message alloc] init];
    msg.content = content;
    msg.time = time;
    msg.icon = @"icon01.png";
    msg.type = MessageTypeMe;
    mf.message = msg;
    
    [_allMessagesFrame addObject:mf];
}

#pragma mark - tableView数据源方法

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _allMessagesFrame.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    MessageCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    if (cell == nil) {
        cell = [[MessageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // 设置数据
    if (_allMessagesFrame.count>=indexPath.row) {
        cell.messageFrame = _allMessagesFrame[indexPath.row];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [_allMessagesFrame[indexPath.row] cellHeight];
}

#pragma mark - 代理方法

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
}
@end
