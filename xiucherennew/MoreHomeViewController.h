//
//  MoreHomeViewController.h
//  xiucherennew
//
//  Created by harry on 13-11-11.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreHomeViewController : UIViewController

- (IBAction)clickMembers:(id)sender;
- (IBAction)clickSixin:(id)sender;
- (IBAction)clickLogout:(id)sender;
@end
