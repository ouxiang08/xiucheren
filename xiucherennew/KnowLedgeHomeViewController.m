//
//  KnowLedgeHomeViewController.m
//  xiucherennew
//
//  Created by harry on 13-11-11.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import "KnowLedgeHomeViewController.h"
#import "KnowLedgeSubCatViewController.h"
@interface KnowLedgeHomeViewController ()
@property(nonatomic,strong)NSMutableArray *cats;
@end

@implementation KnowLedgeHomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    _cats = [[NSMutableArray alloc] init];
    
//    [[UITabBar appearance] setTintColor:[UIColor redColor]];
//    [[UITabBar appearance] setBarTintColor:[UIColor yellowColor]];
    
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *string = [NSString stringWithFormat:@"%@%@%@",WEBSERVICE_URL,WEBSERVICE_KnowledgeHome,[AppDelegate getAppDelegate].userCode];
    
    [manager GET:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        NSString *result = [responseObject objectForKey:@"resultDataType"];
        
        NSLog(@"jsonArray: %@", result);
        
        if([result isEqualToString:@"1"]){
            _cats = [responseObject objectForKey:@"resultList"];
        }
        [self reloadViews];
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

- (void)reloadViews{
    
    for (int i=0; i<[_cats count]; i++) {
        NSDictionary *dic = [_cats objectAtIndex:i];
        
        UIImage *img=[UIImage imageNamed:@"klhome_textbg"];
        UIButton *bt = [[UIButton alloc] initWithFrame:CGRectMake(8,80+i*55,305,45)];
        [bt setImage:img forState:UIControlStateNormal];
        bt.tag = i+1;
        [bt addTarget:self action:@selector(menuClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:bt];

        
        
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(20, 92+i*55,280,21)];
        lblTitle.font = [UIFont systemFontOfSize:17];
        lblTitle.textColor = [UIColor blackColor];
        lblTitle.backgroundColor = [UIColor clearColor];
        lblTitle.opaque = NO;
        lblTitle.textAlignment = NSTextAlignmentCenter;
        lblTitle.text = [dic objectForKey:@"typeName"];
        [self.view addSubview:lblTitle];
    }
}

- (void)menuClick:(UIButton *)bt{
    int tagid = bt.tag;
    NSDictionary *dic = [_cats objectAtIndex:tagid-1];
    
    KnowLedgeSubCatViewController *klvc = [[KnowLedgeSubCatViewController alloc] initWithNibName:@"KnowLedgeSubCatViewController" bundle:nil];
    
    klvc.typeguid = [dic objectForKey:@"typeGuid"];
    klvc.typesname = [dic objectForKey:@"typeName"];
    [self.navigationController pushViewController:klvc animated:YES];
    
}
- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
