//
//  TopicTableViewController.m
//  xiucherennew
//
//  Created by apple on 14-5-14.
//  Copyright (c) 2014年 harry. All rights reserved.
//

#import "TopicTableViewController.h"
#import "PostViewController.h"
#import "UIImageView+WebCache.h"
#import "DetailViewController.h"
#import "Post.h"

@interface TopicTableViewController ()<UITableViewDataSource,UITableViewDelegate>{
    
    int _typeId;
}
@end

@implementation TopicTableViewController

@synthesize topicTitle;
@synthesize topicTableView;
@synthesize topicArray;

-(id)initWithTypeId:(int)typeId{
    
    self = [super init];
    if (self) {
        _typeId = typeId;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (_typeId==1) {
        
        self.topicTitle.text = @"外部话题";
    }else{
        
        self.topicTitle.text = @"内部话题";
    }
}


-(void)viewWillAppear:(BOOL)animated{
    
    [self initData];
}

-(void)initData{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *string = [NSString stringWithFormat:@"%@%@/%@/%d",WEBSERVICE_URL,WEBSERVICE_TopicList,[AppDelegate getAppDelegate].userCode,_typeId];
    
    [manager GET:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        NSString *result = [responseObject objectForKey:@"resultDataType"];
        
        NSLog(@"jsonArray: %@", result);
        
        if([result isEqualToString:@"1"]){
            NSArray *resultlist = [responseObject objectForKey:@"resultList"];
            self.topicArray = [[NSMutableArray alloc] initWithArray:resultlist];
            
            [self.topicTableView reloadData];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }else{
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}
-(IBAction)postAction:(id)sender{
    
    PostViewController *postVC = [[PostViewController alloc] initWithTypeId:_typeId];
    [self.navigationController pushViewController:postVC animated:YES];
}
-(IBAction)backAction:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [topicArray count];
}

/**/
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *topicIdentifier = @"topicIdentifier";
    UITableViewCell *topicCell = [tableView dequeueReusableCellWithIdentifier:topicIdentifier];
    if(topicCell==nil){
        topicCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        topicCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:topicIdentifier];
        
        UIImageView *imageView = [[UIImageView alloc] init];
        imageView.tag = 4101;
        imageView.frame = CGRectMake(6, 20, 75, 60);
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        [topicCell.contentView addSubview:imageView];
        
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.tag = 4102;
        titleLabel.frame = CGRectMake(90, 20, 210, 20);
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.font = [UIFont systemFontOfSize:16.0f];
        titleLabel.textColor = [UIColor blackColor];
        [topicCell.contentView addSubview:titleLabel];
        
        UILabel *contentLabel = [[UILabel alloc] init];
        contentLabel.tag = 4103;
        contentLabel.frame = CGRectMake(90, 40, 210, 40);
        contentLabel.backgroundColor = [UIColor clearColor];
        contentLabel.font = [UIFont systemFontOfSize:14.0f];
        contentLabel.textColor = [UIColor lightGrayColor];
        contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        contentLabel.numberOfLines = 2;
        [topicCell.contentView addSubview:contentLabel];
        
        UILabel *commentLabel = [[UILabel alloc] init];
        commentLabel.tag = 4104;
        commentLabel.frame = CGRectMake(260, 76, 65, 20);
        commentLabel.backgroundColor = [UIColor clearColor];
        commentLabel.font = [UIFont systemFontOfSize:14.0f];
        commentLabel.textColor = [UIColor lightGrayColor];
        [topicCell.contentView addSubview:commentLabel];
        
//        UILabel *typeLabel = [[UILabel alloc] init];
//        typeLabel.tag = 4105;
//        typeLabel.frame = CGRectMake(260, 18, 65, 20);
//        typeLabel.backgroundColor = [UIColor clearColor];
//        typeLabel.font = [UIFont systemFontOfSize:14.0f];
//        typeLabel.textColor = [UIColor lightGrayColor];
//        [topicCell.contentView addSubview:typeLabel];
    }
    
    NSDictionary *dict = [topicArray objectAtIndex:indexPath.row];
    
    UIImageView *imageView = (UIImageView *)[topicCell.contentView viewWithTag:4101];
    UILabel *titleLabel = (UILabel *)[topicCell.contentView viewWithTag:4102];
    UILabel *contentLabel = (UILabel *)[topicCell.contentView viewWithTag:4103];
    UILabel *commentLabel = (UILabel *)[topicCell.contentView viewWithTag:4104];
    //UILabel *typeLabel = (UILabel *)[topicCell.contentView viewWithTag:4105];
    
    [imageView setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"imageContent"]]];
    titleLabel.text = [dict objectForKey:@"typeCode"];
    contentLabel.text = [dict objectForKey:@"wordContent"];
    commentLabel.text = [NSString stringWithFormat:@"%@评论",[dict objectForKey:@"postHuifuNum"]];
    //typeLabel.text = [dict objectForKey:@"postType"];
    return topicCell;
}

#pragma mark - Table view delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 100;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic = [topicArray objectAtIndex:indexPath.row];
    
    Post *post = [[Post alloc] init];
    post.content = [dic objectForKey:@"wordContent"];
    post.nickname = [dic objectForKey:@"postCreaterName"];;
    post.date = [dic objectForKey:@"createTime"];
    post.headerpath = [dic objectForKey:@"userImg"];
    post.postid = [dic objectForKey:@"postGuid"];
    post.zhfnum = [dic objectForKey:@"postZhuanfaNum"];
    post.huifunum = [dic objectForKey:@"postHuifuNum"];
    post.zannum = [dic objectForKey:@"postZanNum"];
    post.imageContent = [dic objectForKey:@"imageContent"];
    post.zanstatus = [[dic objectForKey:@"postInfoUserZanStatus"] intValue];
    post.title = [dic objectForKey:@"typeCode"];

    DetailViewController *detailViewController = [[DetailViewController alloc] init];
    detailViewController.post = post;
    [self.navigationController pushViewController:detailViewController animated:YES];
}


@end
