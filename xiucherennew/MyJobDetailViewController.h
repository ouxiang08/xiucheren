//
//  MyJobDetailViewController.h
//  xiucherennew
//
//  Created by harry on 13-11-15.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyJobDetailViewController : UIViewController
@property(nonatomic,strong)NSString *taskGuid;
- (IBAction)clickBack:(id)sender;

@property (weak, nonatomic) IBOutlet UITextView *txtContent;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@end
