//
//  JobEditViewController.m
//  xiucherennew
//
//  Created by harry on 13-11-16.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import "JobEditViewController.h"

@interface JobEditViewController ()
@property (assign,nonatomic) BOOL isedit;
@property (strong,nonatomic) NSDictionary *curjob;
@end

@implementation JobEditViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _txtContent.editable = NO;
    _isedit = NO;
    
    
    // Do any additional setup after loading the view from its nib.
    //@Path("/getTaskDetail/{taskGuid}")
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *string = [NSString stringWithFormat:@"%@%@%@",WEBSERVICE_URL,WEBSERVICE_JobDetail,_taskguid];
    
   
    
    [manager GET:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        NSString *result = [responseObject objectForKey:@"resultDataType"];
        
        NSLog(@"jsonArray: %@", result);
        
        if([result isEqualToString:@"1"]){
            NSArray *reslist = [responseObject objectForKey:@"resultList"];
            _curjob = [reslist objectAtIndex:0];
            _txtContent.text = [_curjob objectForKey:@"content"];
            _lblTitle.text = [_curjob objectForKey:@"taskName"];
            
            NSMutableArray *unfinish = [[NSMutableArray alloc] initWithObjects:nil];
            NSMutableArray *finish = [[NSMutableArray alloc] initWithObjects:nil];
            
            
            NSArray *personarr = [_curjob objectForKey:@"taskPersons"];
            for (int i=0; i<[personarr count]; i++) {
                NSDictionary *pdic = [personarr objectAtIndex:i];
                int taskstatus = [[pdic objectForKey:@"taskStatus"] intValue];
                if (taskstatus==0) {
                    [unfinish addObject:[pdic objectForKey:@"taskPersonName"]];
                }
                if (taskstatus==1) {
                    [finish addObject:[pdic objectForKey:@"taskPersonName"]];
                }
            }
            
            for (int i=0; i<[unfinish count]; i++) {
                UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, i*20,120,20)];
                lblTitle.font = [UIFont systemFontOfSize:14];
                lblTitle.textColor = [UIColor blackColor];
                lblTitle.backgroundColor = [UIColor clearColor];
                lblTitle.opaque = NO;
                lblTitle.textAlignment = NSTextAlignmentLeft;
                lblTitle.text = [unfinish objectAtIndex:i];
                [_sclUnfinish addSubview:lblTitle];
            }
            
            for (int i=0; i<[finish count]; i++) {
                UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, i*20,120,20)];
                lblTitle.font = [UIFont systemFontOfSize:14];
                lblTitle.textColor = [UIColor blackColor];
                lblTitle.backgroundColor = [UIColor clearColor];
                lblTitle.opaque = NO;
                lblTitle.textAlignment = NSTextAlignmentLeft;
                lblTitle.text = [finish objectAtIndex:i];
                [_sclFinish addSubview:lblTitle];
            }
        }
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
    
    
   
    
    //_lblTitle.text = @"aaaaa";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickEdit:(id)sender {
    if (!_isedit) {
        [_btEdit setImage:[UIImage imageNamed:@"jobsender_save"] forState:UIControlStateNormal];
    }else{
        NSDictionary *dict = @{@"taskGuid": _taskguid,@"taskName": _lblTitle.text,@"publishPerson": [_curjob objectForKey:@"publishPerson"],@"content": _txtContent.text};
        NSString *output = [dict JSONRepresentation];
        
        
        NSMutableArray *resarr = [[NSMutableArray alloc] init];
        NSArray *taskperson = [_curjob objectForKey:@"taskPersons"];
        for (int i=0; i<[taskperson count]; i++) {
            NSDictionary *arow = [taskperson objectAtIndex:i];
            NSString *name = [arow objectForKey:@"taskPerson"];
            NSDictionary *tmp = @{@"taskPerson":name};
            [resarr addObject:tmp];
        }
        if ([resarr count]==0) {
            NSDictionary *tmp = @{@"taskPerson":@""};
            [resarr addObject:tmp];
        }
        NSString *output2 = [resarr JSONRepresentation];
        
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSString *string = [NSString stringWithFormat:@"%@%@%@/%@/%@",WEBSERVICE_URL,WEBSERVICE_UpdateTaskInfoo,[AppDelegate getAppDelegate].userCode,[output urlencode],[output2 urlencode]];
        
        [manager POST:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSLog(@"JSON: %@", responseObject);
            
            NSString *result = [responseObject objectForKey:@"resultDataType"];
            
            NSLog(@"jsonArray: %@", result);
            
            if([result isEqualToString:@"1"])
            {
                [Utils simpleAlert:@"保存成功"];
                [_btEdit setImage:[UIImage imageNamed:@"jobsender_edit"] forState:UIControlStateNormal];
            }else{
                [Utils simpleAlert:[responseObject objectForKey:@"resultInfo"]];
            }
            
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }];
        
    }
    _isedit = !_isedit;
    _txtContent.editable = !_txtContent.editable;
}

- (IBAction)clickBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
