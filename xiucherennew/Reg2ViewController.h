//
//  Reg2ViewController.h
//  xiucherennew
//
//  Created by harry on 13-11-9.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Reg2ViewController : UIViewController<UIAlertViewDelegate,UIPickerViewDelegate, UIPickerViewDataSource,UITextFieldDelegate>{
    UITextField * currentTextfield;//当前的文本筐
    BOOL keyboardIsShown;
    UITextField *txtpass;
    UITextField *txtrepass;
    UITextField *txtrealname;
    UITextField *txtage;
    UITextField *txtidno;
    UITextField *txtgender;
    UITextField *txtbirth;
    UITextField *txtblood;
    
    UITextField *txtqq;
    UITextField *txtmobile;
    UITextField *txtemail;
    UITextField *txtcity;
    UITextField *txtaddress;
    UITextField *txtzipcode;
    UITextField *txtschool;
    UITextField *txtworkexp1;
    UITextField *txtworkexp2;
}
@property (assign,nonatomic)int isedit;
@property (weak, nonatomic) IBOutlet UIImageView *imgvBg;
- (IBAction)clickBack:(id)sender;
@end
