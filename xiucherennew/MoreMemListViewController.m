//
//  MoreMemListViewController.m
//  xiucherennew
//
//  Created by harry on 13-11-16.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import "MoreMemListViewController.h"
#import "MoreMemberDetailViewController.h"

@interface MoreMemListViewController ()
@property (strong, nonatomic)NSArray *mblist;
@property (strong, nonatomic)UITableView *tbview;

@end

@implementation MoreMemListViewController
@synthesize tbview;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    tbview = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    tbview.dataSource = self;
    tbview.delegate = self;
    tbview.rowHeight = 60;
    tbview.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    tbview.frame = CGRectMake(0, 100,
                                 self.tableView.frame.size.width,
                                 self.tableView.frame.size.height);
    [self.view addSubview:tbview];
    
    _mblist = [NSArray arrayWithObjects:@"大毛", @"二毛", @"三毛", nil];
    
   
        self.title = @"成员";
        self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reg1_back.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(back)];
   
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    UIImageView *imgv = [[UIImageView alloc] initWithFrame:self.view.bounds];
    imgv.image = [UIImage imageNamed:@"more_mblist_bg.png"];
    [self.view addSubview:imgv];
    
    self.
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)back {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_mblist count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(70, 21,200,16)];
    lblTitle.font = [UIFont systemFontOfSize:16];
    lblTitle.textColor = [UIColor blackColor];
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.opaque = NO;
    lblTitle.textAlignment = NSTextAlignmentLeft;
    lblTitle.text = [_mblist objectAtIndex:indexPath.row];
    [cell.contentView addSubview:lblTitle];
    
    UIImage *bgimg = [UIImage imageNamed:@"header.png"];
    UIImageView *bgimgv = [[UIImageView alloc] initWithFrame:CGRectMake(15, 15, 30,30)];
    bgimgv.contentMode = UIViewContentModeScaleAspectFill;
    [bgimgv setImage:bgimg];
    [cell.contentView addSubview:bgimgv];
    
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here, for example:
    // Create the next view controller.
    MoreMemberDetailViewController *detailViewController = [[MoreMemberDetailViewController alloc] initWithNibName:@"MoreMemberDetailViewController" bundle:nil];
    
    // Pass the selected object to the new view controller.
    
    // Push the view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
}

/*
 #pragma mark - Navigation
 
 // In a story board-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 
 */


@end
