//
//  JobHomeViewController.m
//  xiucherennew
//
//  Created by harry on 13-11-16.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import "JobHomeViewController.h"
#import "JobEditViewController.h"
#import "JobManViewController.h"
#import "JobSendViewController.h"

@interface JobHomeViewController ()
@property (strong, nonatomic)NSMutableArray *joblist;
@property (strong, nonatomic)NSMutableArray *selectedJob;
@property (strong, nonatomic)NSMutableArray *selectedJobDic;
@end

@implementation JobHomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSArray *arr = [self.navigationController viewControllers];
    NSArray *arr2 = [[NSArray alloc] initWithObjects:[arr objectAtIndex:1], nil];
    [self.navigationController setViewControllers:arr2];
    
    _joblist = [[NSMutableArray alloc] init];
    _selectedJob = [[NSMutableArray alloc] init];
    _selectedJobDic = [[NSMutableArray alloc] init];
    
    [_tbview setAllowsSelection:NO];

    
    /*
    NSArray *arr = [NSArray arrayWithObjects:@"任务1", @"任务2", @"任务3", @"任务4", nil];
    NSDictionary *dic = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:@"2013-11-15",arr,nil] forKeys:[NSArray arrayWithObjects:@"date",@"job",nil]];
    
    NSArray *arr2 = [NSArray arrayWithObjects:@"任务a", @"任务b", @"任务c", nil];
    NSDictionary *dic2 = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:@"2013-11-18",arr2,nil] forKeys:[NSArray arrayWithObjects:@"date",@"job",nil]];
    
    _joblist = [NSArray arrayWithObjects:dic,dic2, nil];
     */
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [_joblist removeAllObjects];
    [_selectedJob removeAllObjects];
    [_selectedJobDic removeAllObjects];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *string = [NSString stringWithFormat:@"%@%@%@",WEBSERVICE_URL,WEBSERVICE_MySendJob,[AppDelegate getAppDelegate].userCode];
    
    [manager GET:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        NSString *result = [responseObject objectForKey:@"resultDataType"];
        
        NSLog(@"jsonArray: %@", result);
        
        if([result isEqualToString:@"1"]){
            //_joblist = [responseObject objectForKey:@"resultList"];
            
            
            NSArray *arr = [responseObject objectForKey:@"resultList"];
            
            
            NSArray *sortedArray = [arr sortedArrayUsingComparator:^(id obj1, id obj2){
                NSDate *date1 = [Utils NSStringDateToNSDate:[obj1 objectForKey:@"createTime"] withFormatter:@"yyyy-MM-dd HH:mm:ss"];
                NSDate *date2 = [Utils NSStringDateToNSDate:[obj2 objectForKey:@"createTime"] withFormatter:@"yyyy-MM-dd HH:mm:ss"];
                return [date2 compare:date1];
            }];
            
            //NSMutableSet *dateset = [[NSMutableSet alloc] init];
            NSMutableArray *dateArray1 = [[NSMutableArray alloc] init];
            for (int i=0; i<[sortedArray count]; i++) {
                NSDictionary *dic = [arr objectAtIndex:i];
                NSString *time = [dic objectForKey:@"createTime"];
                NSArray *tmp = [time componentsSeparatedByString:@" "];
                //[dateset addObject:[tmp objectAtIndex:0]];
                [dateArray1 addObject:[tmp objectAtIndex:0]];
            }
            
            NSMutableArray *dateArray2 = [[NSMutableArray alloc] init];
            for (NSString *obj in dateArray1) {
                if (![dateArray2 containsObject:obj]) {
                    [dateArray2 addObject:obj];
                }
            }
            
            
            for (NSString *string in dateArray2)
            {
                NSMutableArray *jobarr = [[NSMutableArray alloc] init];
                for (int i=0; i<[sortedArray count]; i++) {
                    NSDictionary *dic = [sortedArray objectAtIndex:i];
                    NSString *time = [dic objectForKey:@"createTime"];
                    NSArray *tmp = [time componentsSeparatedByString:@" "];
                    NSString *datestring = [tmp objectAtIndex:0];
                    if ([string isEqualToString:datestring]) {
                        [jobarr addObject:dic];
                    }
                }
                NSDictionary *resdic = @{@"date" :string, @"job" :jobarr};
                [_joblist addObject:resdic];
            }
        }
        [_tbview reloadData];
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickMan:(id)sender {
    if ([_selectedJob count]==0) {
        [Utils simpleAlert:@"请先选择要分配的任务"];
    }else{
        JobManViewController *detailViewController = [[JobManViewController alloc] initWithNibName:@"JobManViewController" bundle:nil];
        detailViewController.jobs = _selectedJobDic;
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
    
}

- (IBAction)clickEdit:(id)sender {
    JobSendViewController *detailViewController = [[JobSendViewController alloc] initWithNibName:@"JobSendViewController" bundle:nil];
   
    [self.navigationController pushViewController:detailViewController animated:YES];
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [_joblist count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSDictionary *dic = [_joblist objectAtIndex:section];
    NSArray *arr = [dic objectForKey:@"job"];
    return [arr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        UIImage *img=[UIImage imageNamed:@"assignjob_unselected.png"];
        UIButton *bt = [[UIButton alloc] initWithFrame:CGRectMake(15,19,img.size.width/2,img.size.height/2)];
        [bt addTarget:self action:@selector(statusClick:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:bt];
        
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(70, 21,200,16)];
        lblTitle.font = [UIFont boldSystemFontOfSize:16];
        lblTitle.textColor = [UIColor blackColor];
        lblTitle.backgroundColor = [UIColor clearColor];
        lblTitle.opaque = NO;
        lblTitle.textAlignment = NSTextAlignmentLeft;
        [cell.contentView addSubview:lblTitle];

        
        UIImage *img2=[UIImage imageNamed:@"viewdetail.png"];
        CGRect imgsize = CGRectMake(cell.frame.size.width-img2.size.width/2,1,img2.size.width/2,img2.size.height/2);
        UIButton *bt2 = [[UIButton alloc] initWithFrame:imgsize];
        [bt2 addTarget:self action:@selector(detailClick:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:bt2];

        
    }
    int count = [cell.contentView.subviews count];
    UIButton *bt = (UIButton *)[cell.contentView.subviews objectAtIndex:count - 3];
    UILabel *lblTitle = (UILabel *)[cell.contentView.subviews objectAtIndex:count - 2];
    UIButton *bt2 = (UIButton *)[cell.contentView.subviews objectAtIndex:count - 1];

    
    NSDictionary *dic = [_joblist objectAtIndex:indexPath.section];
    NSArray *arr = [dic objectForKey:@"job"];
    NSDictionary *dic2 = [arr objectAtIndex:indexPath.row];
    
    
    UIImage *img=[UIImage imageNamed:@"assignjob_unselected.png"];
    [bt setImage:img forState:UIControlStateNormal];
    bt.tag = indexPath.section*100+indexPath.row;
    bt.frame = CGRectMake(15,19,img.size.width/2,img.size.height/2);
    
    UIImage *img2=[UIImage imageNamed:@"viewdetail.png"];
    [bt2 setImage:img2 forState:UIControlStateNormal];
    bt2.tag = indexPath.section*100+indexPath.row;
    bt2.frame = CGRectMake(cell.frame.size.width-img2.size.width/2,1,img2.size.width/2,img2.size.height/2);
    

    lblTitle.tag = 2000+indexPath.section*100+indexPath.row;
    lblTitle.text = [dic2 objectForKey:@"taskName"];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSDictionary *dic = [_joblist objectAtIndex:section];
    NSString *arr = [dic objectForKey:@"date"];
    
    return arr;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        
        NSDictionary *dic = [_joblist objectAtIndex:indexPath.section];
        NSArray *arr = [dic objectForKey:@"job"];
        NSDictionary *dic2 = [arr objectAtIndex:indexPath.row];
        NSString *jobid = [dic2 objectForKey:@"taskGuid"];
        
        //@Path("/deleteTask/{userCode}/{taskGuid}")
        NSString *string = [NSString stringWithFormat:@"%@%@%@/%@",WEBSERVICE_URL,WEBSERVICE_DeleteTask,[AppDelegate getAppDelegate].userCode,jobid];
        
        [manager POST:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSLog(@"JSON: %@", responseObject);
            
            NSString *result = [responseObject objectForKey:@"resultDataType"];
            
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            if([result isEqualToString:@"1"]){
                //_joblist = [responseObject objectForKey:@"resultList"];
                [[[_joblist objectAtIndex:indexPath.section] objectForKey:@"job"] removeObjectAtIndex:indexPath.row];
                
                if ([[[_joblist objectAtIndex:indexPath.section] objectForKey:@"job"] count]==0) {
                    [_joblist removeObjectAtIndex:indexPath.section] ;
                }
                [_tbview reloadData];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@ %@", error,[error localizedDescription]);
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }];
        
        
    }
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

//    JobEditViewController *detailViewController = [[JobEditViewController alloc] initWithNibName:@"JobEditViewController" bundle:nil];
//    
//    NSDictionary *dic = [_joblist objectAtIndex:indexPath.section];
//    NSArray *arr = [dic objectForKey:@"job"];
//    NSDictionary *dic2 = [arr objectAtIndex:indexPath.row];
//    
//    detailViewController.taskguid = [dic2 objectForKey:@"taskGuid"];
//    
//    [self.navigationController pushViewController:detailViewController animated:YES];
    
    
}

- (void)detailClick:(UIButton *)bt{
    int tagid = bt.tag;
    int section = tagid/100;
    int row = tagid-100*section;
    
    JobEditViewController *detailViewController = [[JobEditViewController alloc] initWithNibName:@"JobEditViewController" bundle:nil];
    
    NSDictionary *dic = [_joblist objectAtIndex:section];
    NSArray *arr = [dic objectForKey:@"job"];
    NSDictionary *dic2 = [arr objectAtIndex:row];
    
    detailViewController.taskguid = [dic2 objectForKey:@"taskGuid"];
    
    [self.navigationController pushViewController:detailViewController animated:YES];
}

- (void)statusClick:(UIButton *)bt{
    int tagid = bt.tag;
    int section = tagid/100;
    int row = tagid-100*section;
    
    NSDictionary *dic = [_joblist objectAtIndex:section];
    NSArray *arr = [dic objectForKey:@"job"];
    NSDictionary *dic2 = [arr objectAtIndex:row];
    NSString *jobid = [dic2 objectForKey:@"taskGuid"];
    UIImage *img=[UIImage imageNamed:@"assignjob_selected2"];
    if ([_selectedJob indexOfObject:jobid]==NSNotFound) {
        [_selectedJob addObject:jobid];
        [_selectedJobDic addObject:dic2];
    }else{
        img=[UIImage imageNamed:@"assignjob_unselected"];
        [_selectedJobDic removeObjectAtIndex:[_selectedJob indexOfObject:jobid]];
        [_selectedJob removeObject:jobid];

    }
    
    [bt setImage:img forState:UIControlStateNormal];
    bt.frame = CGRectMake(15,19,img.size.width/2,img.size.height/2);
    
    
    
}

@end
