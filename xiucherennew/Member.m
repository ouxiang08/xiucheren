//
//  Member.m
//  xiucherennew
//
//  Created by harry on 13-11-16.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import "Member.h"

@implementation Member


-(id)initWithDic:(NSDictionary *)dic{
    
    self = [super init];
    if (self) {
        self.memberid = @"";
        self.nickname = [dic objectForKey:@"auserName"];
        self.headerimg = [dic objectForKey:@"userHeadImagePath"];
        self.age = [dic objectForKey:@"auserAge"];
        self.birthyear = [dic objectForKey:@"auserBirthday"];
        self.gender = [[dic objectForKey:@"auserGender"] isEqualToString:@"M"]?@"男":@"女";
        self.mobile = [dic objectForKey:@"auserMobile"];
        self.mail = [dic objectForKey:@"auserMail"];
        self.stationname = [dic objectForKey:@"auorgName"];
        self.stationcode = [dic objectForKey:@"auorgCode"];
    }
    return self;
}
@end
