//
//  CommentView.m
//  xiucherennew
//
//  Created by harry on 14-2-19.
//  Copyright (c) 2014年 harry. All rights reserved.
//

#import "CommentView.h"

@implementation CommentView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame withData:(NSDictionary *)data{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        

        
        if ([data[@"userImg"] isEqual:[NSNull null]]) {
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(12, 8, 26, 26)];
            [imageView setImage:[UIImage imageNamed:@"header"]];
            [self addSubview:imageView];
        }else{
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(12, 8, 26, 26)];
            [imageView setImageWithURL:[NSURL URLWithString:data[@"userImg"]] placeholderImage:nil options:SDWebImageRefreshCached];
            imageView.contentMode = UIViewContentModeScaleAspectFit;
            [self addSubview:imageView];
        }
        
        UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(50, 8, 200, 14)];
        titleLabel.backgroundColor=[UIColor clearColor];
        titleLabel.text=[data[@"postCreater"]  isEqual:[NSNull null]]?@"":data[@"postCreaterName"];
        titleLabel.font = [UIFont systemFontOfSize:14];
        titleLabel.textColor=[UIColor blackColor];
        titleLabel.textAlignment=NSTextAlignmentLeft;
        [self addSubview:titleLabel];
        
        int strlen = [Utils getStrLen:data[@"tieContent"]];
        //NSLog(@"%@ %f",itemInfo.shortAddress,ceilf(strlen/20.0));
        float h = 24*ceil(strlen/20.0);
        if (h==0) {
            h=24;
        }

        UITextView *txtDesc = [[UITextView alloc] initWithFrame:CGRectMake(50, 23,250, h)];
        txtDesc.backgroundColor = [UIColor clearColor];
        txtDesc.textColor = [UIColor blackColor];
        [txtDesc setFont:[UIFont systemFontOfSize:14]];
        txtDesc.editable = NO;
        txtDesc.userInteractionEnabled = NO;
        txtDesc.text = [data[@"tieContent"] isEqual:[NSNull null]]?@"":data[@"tieContent"];
        [self addSubview:txtDesc];
        
        UILabel *dateLabel=[[UILabel alloc] initWithFrame:CGRectMake(50, txtDesc.frame.size.height+txtDesc.frame.origin.y+3, 200, 14)];
        dateLabel.backgroundColor=[UIColor clearColor];
        dateLabel.text=[data[@"createTime"] isEqual:[NSNull null]]?@"":data[@"createTime"];
        dateLabel.font = [UIFont systemFontOfSize:12];
        dateLabel.textColor=[UIColor orangeColor];
        dateLabel.textAlignment=NSTextAlignmentLeft;
        [self addSubview:dateLabel];
        
        UIView *cmtview = [[UIView alloc] initWithFrame:CGRectMake(0, dateLabel.frame.size.height+dateLabel.frame.origin.y+7, frame.size.width, 2)];
        cmtview.backgroundColor = [UIColor lightGrayColor];
        [self addSubview:cmtview];
        
    }
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
