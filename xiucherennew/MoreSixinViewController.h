//
//  MoreSixinViewController.h
//  xiucherennew
//
//  Created by harry on 13-11-16.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreSixinViewController : UIViewController
@property (strong, nonatomic) NSString *nickname;
@property (strong, nonatomic) NSString *usercode;
@property (strong, nonatomic) NSString *headimg;
@property (assign, nonatomic) int uid;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *txtContent;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
- (IBAction)clickCancel:(id)sender;
- (IBAction)clickSend:(id)sender;
@end
